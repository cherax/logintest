//
//  LeftViewController.h
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

//@property (nonatomic, weak) IBOutlet UITableView *table;

@end
