//
//  CenterViewController.m
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import "CenterViewController.h"

@interface CenterViewController ()
{
    NSString *strLogin;
    NSString *strPassword;
}

@end

@implementation CenterViewController

//----------------------------------------------------------------------------

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//----------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.labelLogin setText:strLogin];
    [self.labelPassword setText:strPassword];
}

//----------------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------------------------------------

- (void)setLoginInfoWithLogin:(NSString *)login password:(NSString *)password
{
    strLogin = login;
    strPassword = password;
}

//----------------------------------------------------------------------------

- (IBAction)btnPressed:(id)sender
{
    if( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        //picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        //picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        NSLog(@"Camera not available");
    }
}

//----------------------------------------------------------------------------

@end
