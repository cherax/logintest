//
//  AppDelegate.h
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)loadMainMenuWithLogin:(NSString *)login password:(NSString *)password;

@end
