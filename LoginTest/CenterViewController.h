//
//  CenterViewController.h
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface CenterViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UILabel *labelLogin;
@property (nonatomic, weak) IBOutlet UILabel *labelPassword;

- (void)setLoginInfoWithLogin:(NSString *)login password:(NSString *)password;

@end
