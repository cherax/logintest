//
//  ViewController.m
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize fieldLogin = _fieldLogin;
@synthesize fieldPassword = _fieldPassword;

//--------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.fieldLogin.delegate = self;
    self.fieldPassword.delegate = self;
}

//--------------------------------------------------------------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

//--------------------------------------------------------------------

- (IBAction)loginPressed:(id)sender
{
    if( ![self.fieldPassword.text isEqualToString:@""] && [self validateEmail:self.fieldLogin.text] )
    {
        AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
        [myDelegate loadMainMenuWithLogin:self.fieldLogin.text password:self.fieldPassword.text];
    }
}

//--------------------------------------------------------------------

- (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

//--------------------------------------------------------------------

@end
