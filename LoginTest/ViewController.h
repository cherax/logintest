//
//  ViewController.h
//  LoginTest
//
//  Created by Przemek Jaskot on 26.08.2013.
//  Copyright (c) 2013 Przemek Jaskot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *fieldLogin;
@property (nonatomic, weak) IBOutlet UITextField *fieldPassword;

@end
